#!/usr/bin/python
# -*- coding: utf-8 -*-
# @TODO SET LOG
import hashlib
import json
import os
import shutil
import sys
import pip
import subprocess
from os import path


def look_for_python3():
    """ Detect Python3 on system. """
    print("Check if Python3 is installed")

    has_python = shutil.which("python")

    if has_python is not None:
        print("[OK]")

    else:
        print("[Error]")
        print("Install Python3 and try again.")
        sys.exit(2)


def detect_platform():
    """Detect operating system of host machine."""
    if sys.platform == 'linux' or sys.platform == 'linux2':
        return 'linux'

    elif sys.platform == 'darwin':
        return 'darwin'

    elif sys.platform == 'win32':
        return 'win32'

    else:
        print('Error detecting OS.')
        sys.exit(2)


def generate_yaml_cnf():
    """ Function for generating yaml config file. """

    print("Generating yaml config file...")

    host = input("Insert database host(ex. localhost): ")
    user = input("Insert database username: ")
    password = input("Insert database password: ")
    database = input("Insert database name: ")

    basepath = path.dirname(__file__)
    filepath = path.abspath(path.join(basepath, '.', "config/config.yml"))

    yml = {
        'scraper': {
            'scraper_table': 'scraper_data',
            'scraper_output': path.abspath(path.join(basepath, '.', "./workspace/unprocessed"))
        },
        'mysql': {
            'host': host,
            'port': 3306,
            'database': database,
            'password': password,
            'user': user,
            'use_unicode': 'True',
            'charset': 'utf8'
        }
    }

    with open('./config/config.yml', 'w') as yaml_file:
        yaml.dump(yml, yaml_file, default_flow_style=False)

    print('\nConfig file generated.')


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def get_config(*args):
    """ Get configuration options of parser. """
    base_path = path.dirname(__file__)
    file_path = path.abspath(
        path.join(base_path, "..", "..", "config/config.yml"))
    if file_path.isspace():
        file_path = path.abspath(
            path.join(base_path, "..", "config/config.yml"))
    if file_path.isspace():
        file_path = path.abspath(
            path.join(base_path, "config/config.yml"))

    with open(file_path, 'r') as f:
        conf = yaml.load(f)
        if len(args) > 0:
            # get section
            section = args[0]

            # check if Config file has Section
            if section not in conf:
                print("key missing")
                quit()

            # get values
            argList = list(args)  # convert tuple to list
            argList.pop(0)  # remove Section from list

            # create lookup path
            parsepath = "conf['" + section + "']"

            for arg in argList:
                parsepath = parsepath + "['" + arg + "']"
            return(eval(parsepath))
        else:
            return conf

    f.close()

def generate_md5_hash(string):
    """ Generate md5 from input string """
    # Assumes the default UTF-8
    hash_object = hashlib.md5(string.encode())
    return hash_object.hexdigest()


def install_modules():
    with open('requirements.txt', 'r') as reqs:
        for req in reqs:
            print('Installing: ', req)
            pip.main(['install', req])


def install_os_dependancies():

    if detect_platform() is 'linux':
        subprocess.run("sudo apt-get install python-pip", shell=True, check=True)
        subprocess.run("sudo apt-get install python-dev", shell=True, check=True)
        subprocess.run("sudo apt-get install libmysqlclient-dev", shell=True, check=True)

    if detect_platform() is 'win32':
        print('win32')
        #subprocess.run("python -m pip install python-pip", shell=True, check=True)
        #subprocess.run("python -m pip install python-dev", shell=True, check=True)
        #subprocess.run("python -m pip install libmysqlclient-dev", shell=True, check=True)
        
    else:
        print('Print invalid os quitting.')
        exit()


if __name__ == "__main__":

    look_for_python3()

    install_os_dependancies()

    install_modules()

    import yaml
    import MySQLdb
    import hashlib

    # Set config file
    if not os.path.isfile('./config/config.yml'):
        generate_yaml_cnf()
    else:
        if query_yes_no(question='Config exists overwrite it?', default="yes"):
            generate_yaml_cnf()

    cnf = get_config()
    
    print('\nSetup completed')
