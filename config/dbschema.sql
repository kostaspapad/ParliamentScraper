CREATE TABLE `scraper_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conference_date` date DEFAULT NULL,
  `time_period` varchar(255) DEFAULT NULL,
  `session` varchar(255) NOT NULL,
  `conference_indicator` varchar(255) NOT NULL,
  `video_link` varchar(255),
  `pdf_loc` varchar(255) DEFAULT NULL,
  `pdf_name` varchar(255),
  `doc_location` varchar(255) NOT NULL,
  `doc_name` varchar(255),
  `date_of_crawl` date,
  `morning_conference` int(11) NOT NULL,
  `noon_conference` int(11) NOT NULL,
  `downloaded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
