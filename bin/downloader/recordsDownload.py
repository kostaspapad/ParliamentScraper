# -*- coding: utf-8 -*-
import argparse
import json
import os
import sys
import time
import datetime
import MySQLdb
import requests
from typing import Tuple
from bin.helpers import tools
from bin.helpers import utils
from bin.helpers import connection
# import datetime


# def getAllDocFiles(db_con, cursor, fromID=0):

#     # Connect to sqlite

#     cursor = db_con.execute(
#         "SELECT Date, DocumentLocation, DocumentName FROM vouliData WHERE (Not DocumentName LIKE '%No doc%') and ID > ?", [fromID])

#     # Iterate on results
#     for row in cursor:
#         print(row)
#        # Look for rows containing (morning conference identifier), insert extension doc or docx
#         if len(row[0]) > 10:
#             new_file_name = datetime.datetime.strptime(
#                 row[0][:10], "%d/%m/%Y").strftime("%d-%m-%Y") + "Morning." + row[2].split('.')[1]

#         else:
#             # Change date format and use it as file_name
#             new_file_name = datetime.datetime.strptime(
#                 row[0], "%d/%m/%Y").strftime("%d-%m-%Y") + '.' + row[2].split('.')[1]

#         # Call download function
#         #print(row[1] + '/' + row[2])
#         download(new_file_name)

#         # Delay for next download
#         time.sleep(2)


# def downloadAll(db_con, cursor, start_id=0, output_location='.'):
#     # Connect to sqlite
#     cursor.execute("SELECT ID, Date, DocumentLocation, DocumentName, WebPageNum, MorningConf FROM vouliData WHERE ID = (SELECT MAX(ID) FROM vouliData)")

#     # Iterate on results
#     for row in cursor:
#         print(row)
#     #    # Look for rows containing (morning conference identifier), insert extension doc or docx
#     #     if len(row[0]) > 10:
#     #         new_file_name = datetime.datetime.strptime(
#     #             row[0][:10], "%d/%m/%Y").strftime("%d-%m-%Y") + "Morning." + row[2].split('.')[1]
#        #
#     #     else:
#     #        # Change date format and use it as file_name
#     #        new_file_name = datetime.datetime.strptime(
#     #            row[0], "%d/%m/%Y").strftime("%d-%m-%Y") + '.' + row[2].split('.')[1]
#        #
#     #    # Call download function
#     #    download(row[1] + '/' + row[2], new_file_name)
#        #
#     #    # Delay for next download
#     time.sleep(2)


def download(row_data: Tuple = None, output_folder: str = None):
    """ Download a single document save to unprocessed folder.

    Row example:
     (4886,
      '2017-10-26',
      'UserFiles/a08fc2dd-61a9-4a83-b09a-09f4c564609d',
      '20171026000531.docx',
      0,
      0)
    """
    utils.validate_args((row_data, tuple))
    utils.validate_args((output_folder, str))

    url = 'http://www.hellenicparliament.gr/' + \
        str(row_data[2]) + '/' + str(row_data[3])

    file_name = create_filename(row_data)

    dl = requests.get(url, stream=True)
    if os.path.isdir(output_folder):
        with open(output_folder + '/' + file_name, 'wb') as f:
            for chunk in dl.iter_content(1024 * 1024 * 2):  # 2 MB chunks
                f.write(chunk)

    else:
        print("Output directory does not exist.")
        sys.exit(2)

    time.sleep(1)

def create_filename(row: Tuple = None) -> str:
    if row is None:
        raise ValueError("row is not specified")
    if not isinstance(row, tuple):
        raise TypeError("row is not tuple. Found:", type(row))

    # Get file extension to append at end of new name
    file_extension = row[3].split('.')[1]

    # If is morning conference append morning to name
    if row[4] is 1 and row[5] is 0:
        date = datetime.datetime.strptime(
            row[1], '%Y-%m-%d').strftime('%Y-%m-%d')
        file_name = str(date) + "morning." + file_extension

    elif row[4] is 0 and row[5] is 0:
        date = datetime.datetime.strptime(
            row[1], '%Y-%m-%d').strftime('%Y-%m-%d')
        file_name = str(date) + "." + file_extension

    else:
        file_name = row[1].strftime('%Y-%m-%d') + "." + file_extension

    return file_name


def from_last_conference(output_folder):
    """ Download latest conferences.

    Query db and find what is the latest record by conference date and download
    all documents after that date.
    """

    scraper_table = tools.get_config('fetcher', 'scraper_table')
    output_folder = tools.get_config('fetcher', 'fetcher_output')
    db_con = connection.get_db_connection(with_converters=True)
    database = tools.get_config('mysql', 'database')

    # Change converter value to return dates as strings
    # conv = MySQLdb.converters.conversions.copy()
    # conv[10] = str  # convert dates to strings
    # db_con = MySQLdb.connect(host=cnf['mysql']['host'],
    #                          user=cnf['mysql']['user'],
    #                          passwd=cnf['mysql']['password'],
    #                          db=cnf['mysql']['database'],
    #                          use_unicode=cnf['mysql']['use_unicode'],
    #                          charset=cnf['mysql']['charset'],
    #                          conv=conv)

    cursor = db_con.cursor()

    # Get last conference from speech data
    # If speech_data table is empty
    sql1 = "SELECT date FROM {}.{} ORDER BY date DESC LIMIT 1".format(
        database, scraper_table)

    print(sql1)
    cursor.execute(sql1)
    last_conference_speech_data = cursor.fetchone()
   
    # if last_conference_speech_data is not None:
    sql2 = "SELECT date FROM {}.{} WHERE `date` > '{}'".format(
        database, scraper_table, last_conference_speech_data[0])

    print(sql2)
    cursor.execute(sql2)

    conferences_for_downloading = cursor.fetchall()
   
    if conferences_for_downloading:
        dates = [x[0] for x in conferences_for_downloading]
        escape_helper = ",".join(['%s'] * len(dates))

        cursor.execute(
            """SELECT ID, Date, DocumentLocation, DocumentName, MorningConf, AfternoonConf FROM %s WHERE Date IN (%s)""" % scraper_table, escape_helper,
            tuple(dates))
        
        # cursor.execute(sql3)
        
        # Fetch rows from db
        row_data = cursor.fetchall()

        # Sample of returned data
        # ((4886,
        #   '2017-10-26',
        #   'UserFiles/a08fc2dd-61a9-4a83-b09a-09f4c564609d',
        #   '20171026000531.docx',
        #   0,
        #   0),
        #  (4887,
        #   '2017-10-27',
        #   'UserFiles/a08fc2dd-61a9-4a83-b09a-09f4c564609d',
        #   '20171027000532.docx',
        #   0,
        #   0))

        for row in row_data:
            # Pass row to downloader function
            download(row, output_folder)

        print('Download complete.')
    else:
        print('No new data for download.')

    db_con.close()


def main(argv):
    conn_conf = tools.get_config_info()
    host = conn_conf['scraper_mysql_db']['host']
    user = conn_conf['scraper_mysql_db']['user']
    passwd = conn_conf['scraper_mysql_db']['pass']
    database = conn_conf['scraper_mysql_db']['database']
    charset = conn_conf['scraper_mysql_db']['charset']
    use_unicode = conn_conf['scraper_mysql_db']['use_unicode']

    db_con = MySQLdb.connect(host=host,
                             user=user,
                             passwd=passwd,
                             db=database,
                             use_unicode=use_unicode,
                             charset=charset)

    cursor = db_con.cursor()

    parser = argparse.ArgumentParser(
        description="Greek parliament transcript scrapper")

    parser.add_argument(
        "-d", "--downloadlast", help="Download last inserted doc", action='store_true')

    parser.add_argument(
        "-f", "--fromID", help="Download single file specified by ID in database")

    parser.add_argument(
        "-a", "--all", help="Download all doc files", action='store_true')

    parser.add_argument(
        "-o", "--output", help="Output folder", required=True)

    args = vars(parser.parse_args())

    if args['downloadlast']:
        # Rrepare query for selection of last row
        cursor.execute(
            "SELECT ID, Date, DocumentLocation, DocumentName, MorningConf, AfternoonConf FROM vouliData WHERE ID = (SELECT MAX(ID)  FROM vouliData) LIMIT 1")

        # Fetch last row from db
        row_data = cursor.fetchall()

        # Pass row data to downloader function
        download(row_data, args['output'])

    if args['fromID']:
        start_id = args['fromID']
        print("Start from id %s" % start_id)

        # Rrepare query for selection of data after specified id
        cursor.execute(
            "SELECT ID, Date, DocumentLocation, DocumentName, MorningConf, AfternoonConf FROM vouliData WHERE ID >= %s",
            (start_id,))

        # Fetch all rows from db starting from a specified id
        row_data = cursor.fetchall()

        # Start timer
        start_time = time.time()

        # Pass row data to downloader function
        download(row_data, args['output'])

        # Stop timer, Get time passed for downloading
        print("--- %s seconds ---" % (time.time() - start_time))

    if args['all']:
        print("Download all files")

        # Rrepare query for selection of all data
        cursor.execute(
            "SELECT ID, Date, DocumentLocation, DocumentName, MorningConf, AfternoonConf FROM vouliData")

        # Fetch all rows from db
        row_data = cursor.fetchall()

        # Start timer
        start_time = time.time()

        # Fetch all rows from db
        download(row_data, args['output'])

        # Stop timer, Get time passed for downloading
        print("--- %s seconds ---" % (time.time() - start_time))

    db_con.close()


if __name__ == "__main__":
    req_version = (3, 0)
    cur_version = sys.version_info

    if cur_version >= req_version:
        main(sys.argv[1:])
    else:
        print("Your Python interpreter is not valid. Please consider python 3.")
