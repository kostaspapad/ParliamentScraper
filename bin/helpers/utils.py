# -*- coding: utf-8 -*-
""" Some usefull functions"""
import json
import os
import sys
from typing import List
from typing import Tuple
from . import connection
import yaml
import psutil
from . import tools
from os import path
import time
 

debug_mode = tools.get_config('scraper', 'debug_mode')
def timeit(method):
    def timed(*args, **kw):
        """Decorator for calculating the execution time of functions"""       
        if debug_mode:
            ts = time.time()
            result = method(*args, **kw)
            te = time.time()
            print(method.__module__, method.__name__, round((te -ts)*1000,1), 'ms')
            return result
    return timed
# def timeit(method):
#     def timed(*args, **kw):
#         if debug_mode:
#             ts = time.time()
#             result = method(*args, **kw)
#             te = time.time()

#             if 'log_time' in kw:
#                 name = kw.get('log_name', method.__name__.upper())
#                 kw['log_time'][name] = int((te - ts) * 1000)
#             else:
#                 print('%r  %2.2f ms' % \
#                     (method.__name__, (te - ts) * 1000))
#             return result
#         else:
#             return

#     return timed

def get_mem_usage():
    pid = os.getpid()
    py = psutil.Process(pid)
    memory_use = py.memory_info()[0] / 2. ** 30  # memory use in GB...I think
    print('memory use:', memory_use)


def validate_single_var(var_tuple: Tuple = None) -> bool:
    if var_tuple is None:
        raise ValueError("argument is not specified")
    if not isinstance(var_tuple, tuple):
        raise TypeError("argument must be a tuple")
    if len(var_tuple) == 0:
        raise ValueError("argument must not be an empty tuple")
    if len(var_tuple) != 2:
        raise ValueError("tuple must have length of 2")

    if not isinstance(var_tuple[0], var_tuple[1]):
        raise TypeError("Invalid type {}. Type must be {} "
                        .format(type(var_tuple[0]), var_tuple[1]))

    return True


def validate_args(var_type_tuple=None):
    if var_type_tuple is None:
        raise ValueError("{} is not specified".format(var_type_tuple))
    if not isinstance(var_type_tuple[0], var_type_tuple[1]):
        raise TypeError("Invalid type {}. Type must be {} ".format(
            type(var_type_tuple[0]), var_type_tuple[1]))
    return True


def list_common_elements(list1: List[str], list2: List[str]) -> List[str]:
    """ Return the common elements of a list """
    return list(set(list1).intersection(list2))


def detect_platform() -> str:
    """Detect operating system of host machine."""
    if sys.platform == 'linux' or sys.platform == 'linux2':
        return 'linux'

    elif sys.platform == 'darwin':
        return 'darwin'

    elif sys.platform == 'win32':
        return 'win32'

    else:
        print('--->Error detecting OS.')
        sys.exit(2)


def get_config_info():
    """Get confing info for mysql connections."""
    if detect_platform() is 'linux':
        # Open and parse config json file
        with open('./config/config_linux.json', 'r') as cnf_linux:
            return json.load(cnf_linux)
    else:
        print("--->Platform is not linux. Terminating.")
        sys.exit(2)



def get_config(*args):
    """ Get configuration options of parser. """
    base_path = path.dirname(__file__)
    file_path = path.abspath(
        path.join(base_path, "..", "..", "config/config.yml"))

    with open(file_path, 'r') as f:
        conf = yaml.load(f)
        if len(args) > 0:
            # get section
            section = args[0]

            # check if Config file has Section
            if section not in conf:
                print("key missing")
                quit()

            # get values
            argList = list(args)  # convert tuple to list
            argList.pop(0)  # remove Section from list

            # create lookup path
            parsepath = "conf['" + section + "']"

            for arg in argList:
                parsepath = parsepath + "['" + arg + "']"
            return(eval(parsepath))
        else:
            return conf

    f.close()


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def check_scraper_folder_structure():
    scraper_output = get_config('scraper', 'scraper_output')
    scraper_pdf_output = get_config('scraper', 'scraper_pdf_output')

    if os.path.isdir(scraper_output):
        print('scraper_output: OK') 
    else:
        print('scraper_output: ERR')
        exit()

    if os.path.isdir(scraper_pdf_output):
        print('scraper_pdf_output folder: OK') 
    else:
        print('scraper_pdf_output: ERR')
        exit()