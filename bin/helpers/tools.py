# -*- coding: utf-8 -*-
""" Some usefull functions"""
import json
import sys
from typing import List
from os import path
import yaml


def list_common_elements(list1: List[str], list2: List[str]) -> List[str]:
    """ Return the common elements of a list """
    return list(set(list1).intersection(list2))


def detect_platform() -> str:
    """Detect operating system of host machine."""
    if sys.platform == 'linux' or sys.platform == 'linux2':
        return 'linux'

    elif sys.platform == 'darwin':
        return 'darwin'

    elif sys.platform == 'win32':
        return 'win32'

    else:
        print('--->Error detecting OS.')
        sys.exit(2)


# def get_config_info():
#     """Get config info for mysql connections."""
#     if detect_platform() is 'linux':
#         # Open and parse config json file
#         with open('./config/config_linux.json', 'r') as cnf_linux:
#             return json.load(cnf_linux)
#     else:
#         print("--->Platform is not linux. Terminating.")
#         sys.exit(2)


# def get_config_info_yaml(value):
#     base_path = path.dirname(__file__)
#     file_path = path.abspath(
#         path.join(base_path, "..", "..", "config/config.yml"))

#     with open(file_path, 'r') as yml_file:
#         return yaml.load(yml_file)


def get_config(*args):
    """ Get configuration options of parser. """
    base_path = path.dirname(__file__)
    file_path = path.abspath(
        path.join(base_path, "..", "..", "config/config.yml"))
    # print("config file path:", file_path)
    
    with open(file_path, 'r') as f:
        conf = yaml.load(f)
        if len(args) > 0:
            # get section
            section = args[0]
            
            # check if Config file has Section
            if section not in conf:
                print("key missing")
                quit()

            # get values
            argList = list(args)  # convert tuple to list
            argList.pop(0)  # remove Section from list

            # create lookup path
            parsepath = "conf['" + section + "']"

            for arg in argList:
                parsepath = parsepath + "['" + arg + "']"

            return(eval(parsepath))
            
        else:
            return conf

    f.close()
