# -*- coding: utf-8 -*-
"""
Helper module for functions that have to do with database connections.
"""

# import MySQLdb
# from MySQLdb.converters import conversions
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
from bin.helpers import tools


def get_db_connection(with_converters=False):
    """ Get connection info, connect to db and return connection object.

        Returns:
            db_con: A connection object for mysql
    """
    # Get connection conf info
    conn_conf = tools.get_config()

    host = conn_conf['mysql']['host']
    user = conn_conf['mysql']['user']
    passwd = conn_conf['mysql']['password']
    database = conn_conf['mysql']['database']
    charset = conn_conf['mysql']['charset']
    use_unicode = conn_conf['mysql']['use_unicode']
    
    # if with_converters:
    #     # Change converter value to return dates as strings
    #     conv = MySQLdb.converters.conversions.copy()
    #     conv[10] = str  # convert dates to strings

    #     db_con = MySQLdb.connect(host=host,
    #                              user=user,
    #                              passwd=passwd,
    #                              db=database,
    #                              use_unicode=use_unicode,
    #                              charset=charset,
    #                              conv=conv)
    # else:
    #     db_con = MySQLdb.connect(host=host,
    #                             user=user,
    #                             passwd=passwd,
    #                             db=database,
    #                             use_unicode=use_unicode,
    #                             charset=charset)
    try:
        return mysql.connector.connect(user=user, password=passwd,
                              host=host,
                              database=database)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    # return db_con
