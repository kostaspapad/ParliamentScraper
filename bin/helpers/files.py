# encoding=utf-8
import glob
import json
import shutil
from typing import List, Set

from bin.helpers import utils



def get_files_from_folder(path: str = None, f_types: tuple = ('*.docx', '*.doc', '*.txt')) -> List:
    """Find xml files in folder.

    Search for xml in folder specified in parameter. Return list of
    names.

    Args:
        path:The folder path to search for files.

    Returns:
        A list with the file names.
    """
    utils.validate_single_var((path, str))
    #utils.validate_single_var((f_types, str))

    types = ('*.docx', '*.doc', '*.txt')

    files_grabbed = []

    for files in types:
        files_grabbed.extend(glob.glob(path + "/" + files))

    if files_grabbed:
        return files_grabbed
    else:
        print("Files not found in path:", path)


def move_file(src, dest):
    """ Copy src files to dest. """
    utils.validate_single_var((src, str))
    utils.validate_single_var((dest, str))

    if src.endswith(".txt") or src.endswith(".doc") or src.endswith(".docx"):
        shutil.move(src, dest)


def open_json(f_name):
    """ Parse json file. """
    utils.validate_single_var((f_name, str))
    with open(f_name, 'r') as j:
        data = j.read()

    return json.loads(data)


def read_xml_file(self, f_name):
    """Read an xml file specified in argument.

    Returns:
        A string with the data of the xml file.

    Raises:
        FileNotFoundError
    """
    utils.validate_single_var((f_name, str))
    try:
        with open(f_name, 'r') as f:
            text = f.read()
        return text

    except FileNotFoundError as e:
        print("File not found")
        raise(e)


def save_as_xml(file_name_path: str = None, body: str = None):
    """ Save text as xml file"""
    utils.validate_single_var((file_name_path, str))
    utils.validate_single_var((body, str))

    with open(file_name_path, 'w') as f:
        f.write(body)


def open_list(path: str = None) -> List[str]:
    """ Open list txt file as list """
    utils.validate_single_var((path, str))
    list = []
    try:
        with open(path, 'r') as lst:
            for line in lst.readlines():
                list.append(line.strip('\n'))
    except FileNotFoundError:
        raise

    return list


def open_list_as_set(path: str = None) -> Set[str]:
    """ Open list txt file as set """
    utils.validate_single_var((path, str))

    temp_set = set()

    with open(path, 'r') as lst:
        for line in lst.readlines():
            temp_set.add(line.strip('\n'))

    return temp_set
