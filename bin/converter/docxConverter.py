# encoding=utf-8
""" Convert ms word documents to text. """
import logging
from subprocess import Popen, PIPE

#import bin.converter.extract_text as ex_text
from docx import Document
# from bin.helpers import utils

class DocxConverter():

    # @utils.timeit
    def extract_text(self, filename: str = None) -> str:
        if not filename:
            raise ValueError('file name not specified')

        if filename[-4:] == ".doc":
            return self.convert_doc_to_text(filename)
        elif filename[-5:] == ".docx":
            return self.convert_docx_to_text(filename)
        elif filename[-4:] == ".txt":
            return self.read_txt(filename)
        else:
            return 'error'

    # @utils.timeit
    def convert_doc_to_text(self, file_path: str = None) -> str:
        if not file_path:
            raise ValueError('file path not specified')

        cmd = ['antiword', file_path]
        p = Popen(cmd, stdout=PIPE)
        stdout, stderr = p.communicate()
        return stdout.decode('utf8', 'ignore')

    # @utils.timeit
    def read_txt(self, filename: str = None) -> str:
        if not filename:
            raise ValueError('file name not specified')

        with open(filename, 'r') as f:
            return f.read()

    # @utils.timeit
    def convert_docx_to_text(self, filename: str = None) -> str:
        if not filename:
            raise ValueError('file name not specified')
        
        doc = Document(filename)
        fullText = []
        for para in doc.paragraphs:
            fullText.append(para.text)
        return '\n'.join(fullText)