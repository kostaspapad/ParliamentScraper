# -*- coding: utf-8 -*-
# TODO Lipoun data sto row 736 na to kita3o!!
# Cronjob CPANEL 
# Scraper
#   source /home/greekparliament/virtualenv/parser/3.7/bin/activate && python3 /home/greekparliament/parser/ParliamentScraper/run.py
# Parser
#   source /home/greekparliament/virtualenv/parser/3.7/bin/activate && python3 /home/greekparliament/parser/GreekParliament/run.py
import os
import re
import argparse
import datetime
import pprint
import sys
import shutil
import time
import warnings
from urllib.parse import urlparse
from typing import List
from typing import Tuple
from bin.converter import docxConverter
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import requests
from bs4 import BeautifulSoup
from bin.helpers import connection
from bin.helpers import tools
from bin.helpers import utils
from bin.helpers import files

class Scraper:
    def __init__(self) -> None:
        print("Init crawler...")
        utils.check_scraper_folder_structure()

        # Get configuration from config yaml file
        self.database = tools.get_config('mysql', 'database')
        self.scraper_table = tools.get_config('scraper', 'scraper_table')
        self.top_questions_c1_table = tools.get_config('scraper', 'top_questions_c1_table')
        self.top_questions_c2_table = tools.get_config('scraper', 'top_questions_c2_table')
        self.references_questions_table = tools.get_config('scraper', 'references_questions_table')
        self.output_folder = tools.get_config('scraper', 'scraper_output')

        # Initialize connection to database
        self.db_con = connection.get_db_connection(with_converters=False)
        self.cursor = self.db_con.cursor()

        # Initialize document converter
        self.converter = docxConverter.DocxConverter()

    def import_top_questions_to_db(self) -> None:
        """Επικερες ερωτήσεις κύκλου 1 κύκλου 2"""
        if self.db_con is None:
            raise ValueError("db_con object not specified")

        if self.cursor is None:
            raise ValueError("db cursor object not specified")
    
    def crawl_all_top_questions(self) -> None:
        print('Start crawling all top question data...')

        page_num = Scraper.get_number_of_pages_top_questions()
        
        print('Number of pages:', page_num)

        start_time = time.time()

        # Loop for number of pages - 1
        for page_number in range(page_num, 0, -1):
            self.crawl_top_questions_page(page_number)

        print("Crawling --- %s seconds ---" % (time.time() - start_time))

    def import_conferences_to_db(self) -> None:
        if self.db_con is None:
            raise ValueError("db_con object not specified")

        if self.cursor is None:
            raise ValueError("db cursor object not specified")

        try:
            sql = """ INSERT INTO {db}.conferences
                      SELECT id,
                        conference_date,
                        conference_indicator,
                        doc_location,
                        doc_name,
                        video_link,
                        `session`,
                        date_of_crawl,
                        pdf_loc,
                        pdf_name,
                        time_period,
                        downloaded
                      FROM {db}.scraper_data
                      WHERE {db}.scraper_data.conference_date NOT IN (
                          SELECT {db}.conferences.conference_date FROM {db}.conferences); """.format(db=self.database)
            
            self.cursor.execute(sql)
            self.db_con.commit()

        except mysql.connector.Error as err:
            print(self.cursor._last_executed)
            print("Something went wrong: {}".format(err))

    def crawl_all_conferences(self) -> None:
        """ Crawl hellenic parliament's website and crawl all conference tables. """
        print('Start crawling all data...')

        page_num = Scraper.get_number_of_pages_conferences()

        print("Crawling all pages...")
        print('Number of pages:', page_num)

        start_time = time.time()

        # Loop for number of pages - 1
        for page_number in range(page_num, 0, -1):
            self.crawl_conference_page(page_number)

        print("Crawling --- %s seconds ---" % (time.time() - start_time))

    def crawl_for_new(self):

        # get last date record
        # self.cursor.execute(
        #     """SELECT conference_date FROM scraper_data""")

        # last_date = self.cursor.fetchall()
        # print('Last inserted conferences in: ', last_date)

        print('Start crawling for new conferences...')

        for i in range(0, 5):
            self.crawl_conference_page(i)
        for i in range(0, 1):
            self.crawl_top_questions_page(page_number=i)

    def crawl_top_questions_page(self, page_number: int=None) -> None:
        if page_number is None:
            raise ValueError("No page number scecified")

        if not isinstance(page_number, int):
            raise TypeError("page_number is not integer. Found: ",
                            type(page_number))

        TOP_QUESTIONS_URL = "https://www.hellenicparliament.gr/Koinovouleftikos-Elenchos/Deltio-Epikairon-Erotiseon?search=on&pageNo="

        # Insert page number to url
        url = '%s=%d' % (TOP_QUESTIONS_URL, page_number)

        print('---:> parse page with date links to questions...\n   ', url)
        table = Scraper.get_top_questions_page_bs4_table(url)

        # i date link and extract data
        self.loop_newsletter_dates(table)
        

    def crawl_conference_page(self, page_number: int=None) -> None:
        """ Crawl page and return its data."""
        if page_number is None:
            raise ValueError("No page number scecified")

        if not isinstance(page_number, int):
            raise TypeError("page_number is not integer. Found: ",
                            type(page_number))

        PARLIAMENT_URL = "http://www.hellenicparliament.gr/Praktika/Synedriaseis-Olomeleias?pageNo"

        # Insert page number to url
        url = '%s=%d' % (PARLIAMENT_URL, page_number)

        print('---:> parse page...\n   ', url)
        table = Scraper.get_conference_page_bs4_table(url)
        
        print('---:> extract data...')
        list_row_data = Scraper.parse_conference_info(table)

        if list_row_data:
            print('---:> insert to db...')
            for row in list_row_data:
                self.insert_conference_to_db(row)

        else:
            print('No data return')

    def insert_conference_to_db(self, data) -> None:
        """ Insert conference data to database. """
        if self.db_con is None:
            raise ValueError("db_con object not specified")

        if self.cursor is None:
            raise ValueError("db cursor object not specified")

        if data is None:
            raise ValueError("data not specified")



        try:
            print('Check if %s exists in db...' % data['conference_date'])

            check_if_exist_query = """ SELECT conference_date FROM {} WHERE conference_date = "{}";""".format(self.scraper_table, data['conference_date'])

            self.cursor.execute(check_if_exist_query)

            conference_exist = self.cursor.fetchall()


            # If conference does not exist continue else break
            if not conference_exist:
                print('Conference date does not exist in db')
                print("""---------------
    conference_date: {}
    time_period: {}
    session: {}
    conference_indicator: {}
    video_link: {}
    pdf_loc: {}
    pdf_name: {}
    doc_location: {}
    doc_name: {}
    date_of_crawl: {}
    morning_conference: {}
    noon_conference: {}
    downloaded: {}
----------------""".format(data['conference_date'],data['time_period'],data['session'],
                        data['conference_indicator'],data['video_link'],data['pdf_loc'],
                        data['pdf_name'],data['doc_location'],data['doc_name'],
                        data['date_of_crawl'],data['morning_conference'],
                        data['noon_conference'],data['downloaded']
                    ))
                insert_data = (
                            data['conference_date'],
                            data['time_period'],
                            data['session'],
                            data['conference_indicator'],
                            data['video_link'],
                            data['pdf_loc'],
                            data['pdf_name'],
                            data['doc_location'],
                            data['doc_name'],
                            data['date_of_crawl'],
                            data['morning_conference'],
                            data['noon_conference'],
                            data['downloaded']
                        )
               
                sql = """ INSERT INTO {} (`conference_date`,`time_period`,`session`,`conference_indicator`,`video_link`,`pdf_loc`,`pdf_name`,`doc_location`,`doc_name`,`date_of_crawl`,`morning_conference`,`noon_conference`,`downloaded`)
                            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);""".format(self.scraper_table)

                self.cursor.execute(sql, insert_data)
                self.db_con.commit()
            else:
                print("Conference already in database.")

        except mysql.connector.Error as err:
            print("Something went wrong: {}".format(err))

    def download_not_downloaded(self) -> None:
        """ Look for field `downloaded in db`, get fields with value `0` """
        # max([x.split('/')[2].split('.')[0] for x in files.get_files_from_folder('workspace/unprocessed')])
        print('Download not downloded...')

        sql2 = "SELECT conference_date FROM {}.{} WHERE downloaded = 0".format(
            self.database, self.scraper_table)
        # print(sql2)
        # exit()
        self.cursor.execute(sql2)

        conferences_for_downloading = self.cursor.fetchall()
        
        if not conferences_for_downloading:
            print('No conferences in database.')
            exit()

        dates = [x[0] for x in conferences_for_downloading]
        escape_helper = ",".join(['%s'] * len(dates))

        q = """SELECT
                id,
                conference_date,
                doc_location,
                doc_name,
                pdf_loc,
                pdf_name
                FROM {}.{}""".format(self.database, self.scraper_table)
        self.cursor.execute(q + """ WHERE conference_date IN (%s)
                                    AND doc_name != 'No doc'""" % escape_helper, tuple(dates))
        
        row_data = self.cursor.fetchall()

        print("\n{} Documents for downloading...\n".format(len(row_data)))        
        for row in row_data:
            
            # Look for doc
            if(row[2] != 'No doc loc' and row[3] != 'No doc'):
                # Pass row to downloader function
                print('Downloading... ', row)
                self.download(row)

            # elif(row[4] != 'No pdf loc' and row[5] != 'No pdf'):
            #     print("NOT IMPLEMENTED!")
            #     print('downloading PDF... ', row)
                #self.download_pdf(row)

            else:
                print('No files found')
                pass

        print('Download complete.')

    def download(self, row_data: tuple=None) -> None:
        """ Download a single document save to unprocessed folder.

        row_data example:
        (4886,
        '2017-10-26',
        'UserFiles/a08fc2dd-61a9-4a83-b09a-09f4c564609d',
        '20171026000531.docx',
        0,
        0)
        """
        utils.validate_args((row_data, tuple))
        utils.validate_args((self.output_folder, str))

        url = 'http://www.hellenicparliament.gr/' + \
            str(row_data[2]) + '/' + str(row_data[3])

        file_name = Scraper.create_filename(row_data)

        #update_query = 'UPDATE parliament.scraper_data SET downloaded=1 WHERE id={}'.format(row_data[0])
        #print(update_query)

        dl = requests.get(url, stream=True)

        if os.path.isdir(self.output_folder):
            with open(self.output_folder + '/' + file_name, 'wb') as f:
                for chunk in dl.iter_content(1024 * 1024 * 2):  # 2 MB chunks
                    f.write(chunk)

                self.cursor.execute("""UPDATE %s.%s SET downloaded=1 WHERE id=%s""" % (self.database, self.scraper_table, row_data[0] ))
                self.db_con.commit()

                print('Downloaded: ', file_name)

        else:
            print("Output directory does not exist.")
            sys.exit(2)

        time.sleep(2)

    @staticmethod
    def create_filename(row: tuple=None) -> str:
        if row is None:
            raise ValueError("row is not specified")

        if not isinstance(row, tuple):
            raise TypeError("row is not tuple. Found:", type(row))

        # Get file extension to append at end of new name
        file_extension = row[3].rsplit('.', 1)[1]

        # If is morning conference append morning to name
        # morning = row[6]
        # afternoon = row[7]
        #if row[6] is 1 and row[7] is 0:
        #    file_name = '{:%Y-%m-%d}morning.{}'.format(row[1], file_extension)
        #elif row[6] is 0 and row[7] is 1:
        #    file_name = '{:%Y-%m-%d}noon.{}'.format(row[1], file_extension)
        #else:

        file_name = '{:%Y-%m-%d}.{}'.format(row[1], file_extension)

        return file_name

    @staticmethod
    def parse_conference_info(table=None) -> List:
        if table is None:
            raise ValueError("No table scecified")

        # A list that holds dictionaries
        list_row_data = []

        # Find all the <tr> tag pairs, skip the first two and last one, then for each.
        for row in reversed(table.find_all('tr')[2:-1]):

            # Create dict for row data, later used in sql query
            data = {'conference_date': '',
                    'time_period': '',
                    'session': '',
                    'conference_indicator': '',
                    'video_link': '',
                    'pdf_loc': '',
                    'pdf_name': '',
                    'doc_location': '',
                    'doc_name': '',
                    'date_of_crawl': datetime.datetime.now().strftime("%Y/%m/%d"),
                    'morning_conference': 0,
                    'noon_conference': 0,
                    'downloaded': 0}

            # Create a variable of all the <td> tag pairs in each <tr> tag pair,
            col = row.find_all('td')

            temp_date = col[0].string.strip()

            print(temp_date)

            # Check if there is a morning Conference and raise flag
            if '(πρωί)' in temp_date:
                data['morning_conference'] = 1
                data['noon_conference'] = 0

                # r_date is the Date found in row of <td>
                r_date = Scraper.convert_to_date_obj(temp_date.split(' ')[0])

            elif '(απόγευμα)' in temp_date:
                data['morning_conference'] = 0
                data['noon_conference'] = 1

                # r_date is the Date found in row of <td>
                r_date = Scraper.convert_to_date_obj(temp_date.split(' ')[0])

            else:
                data['morning_conference'] = 0
                data['noon_conference'] = 0

                # r_date is the date found in row of <td>
                r_date = Scraper.convert_to_date_obj(temp_date)

            # Change format of Date from "1989-12-01" to "1989/12/01"
            r_date = datetime.datetime.strftime(r_date, "%Y/%m/%d")

            # Get 11 chars of Date
            data['conference_date'] = r_date

            if col[1].get_text():
                data['time_period'] = col[1].string.strip()

            else:
                data['time_period'] = 'Empty'

            if col[2].get_text():
                data['session'] = col[2].string.strip()

            else:
                data['session'] = 'Empty'

            if col[3].get_text():
                data['conference_indicator'] = col[3].string.strip()

            else:
                data['conference_indicator'] = 'Empty'

            if col[4].find('a') is not None:
                data['video_link'] = col[4].find('a').get('href')

            else:
                data['video_link'] = 'No video'

            # PDF DOCUMENT

            if col[5].find('a') is not None:
                # Split location on '/' char to get location and file
                pdflink = col[5].find('a').get('href').split('/')
                data['pdf_loc'] = pdflink[1] + '/' + pdflink[2]
                data['pdf_name'] = pdflink[3]

            else:
                data['pdf_loc'] = 'No pdf loc'
                data['pdf_name'] = 'No pdf'

            if col[6].find('a') is not None:
                doclink = col[6].find('a').get('href').split('/')
                data['doc_location'] = doclink[1] + '/' + doclink[2]
                data['doc_name'] = doclink[3]

            else:
                data['doc_location'] = 'No doc loc'
                data['doc_name'] = 'No doc'

            list_row_data.append(data)

        if list_row_data:
            return list_row_data

    def loop_newsletter_dates(self, table=None) -> List:
        if table is None:
            raise ValueError("No table scecified")

        complete_qst_lst = []

        # Find all the <tr> tag pairs, skip the first two and last one, then for each.
        for row in reversed(table.find_all('li')):
            
            # Find all links for the files in the list
            for col in row.find_all('a'):
                # href = re.search('href=\"(\/Koinovouleftikos\-Elenchos.*?)\"\sid=', col[0])
                link = "https://www.hellenicparliament.gr" + col.get('href')
                newsletter_date = Scraper.convert_to_date_obj(col.text)
                
                print('------:> {} questions...\n'.format(newsletter_date))
                self.parse_and_insert_questions(newsletter_date, link)

    def parse_and_insert_questions(self, newsletter_date=None, link=None) -> None:
        if not newsletter_date:
            raise ValueError("No newsletter date specified")
        if not link:
            raise ValueError("No link specified")
        
        # Get adl_id from link (its used instead of full url to questions)
        try:
            adl_id = re.search('adl_id=([a-z0-9\-]+$)', link).group(1)
        except AttributeError:
            adl_id = ''
            
        # Scrape the HTML at the url
        r = requests.get(link, timeout=20)

        if not r.text:
            raise ValueError('Could not download html')
        
        # Turn the HTML into a Beautiful Soup object
        soup = BeautifulSoup(r.text, 'lxml')

        """ Group cases
            a100, b100, c100
            a101, b101
            a110, c101
            b110, c110
            a111
            b111
            c111"""
        wrapper_regex = '(Α\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Πρώτου\s+Κύκλου\s+(?:\(.*?\))(?P<a100>.*)Β\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Δεύτερου\s+Κύκλου\s+(?:\(.*?\))(?P<b100>.*)?ΑΝΑΦΟΡΕΣ-ΕΡΩΤΗΣΕΙΣ (?:\(.*?\))(?P<c100>.*) |Α\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Πρώτου\s+Κύκλου\s+(?:\(.*?\))(?P<a101>.*)Β\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Δεύτερου\s+Κύκλου\s+(?:\(.*?\))(?P<b101>.*)?|Α\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Πρώτου\s+Κύκλου\s+(?:\(.*?\))(?P<a110>.*)ΑΝΑΦΟΡΕΣ-ΕΡΩΤΗΣΕΙΣ (?:\(.*?\))(?P<c101>.*)|Β\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Δεύτερου\s+Κύκλου\s+(?:\(.*?\))(?P<b110>.*)?ΑΝΑΦΟΡΕΣ-ΕΡΩΤΗΣΕΙΣ (?:\(.*?\))(?P<c110>.*)|Α\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Πρώτου\s+Κύκλου\s+(?:\(.*?\))(?P<a111>.*)|Β\.\s+ΕΠΙΚΑΙΡΕΣ\s+ΕΡΩΤΗΣΕΙΣ\s+Δεύτερου\s+Κύκλου\s+(?:\(.*?\))(?P<b111>.*)?|ΑΝΑΦΟΡΕΣ-ΕΡΩΤΗΣΕΙΣ (?:\(.*?\))(?P<c111>.*))'
        wrapper_questions = re.search(wrapper_regex, soup.text, re.DOTALL)

        # question extractor regex for for all types of questions
        extract_regex = '(?P<number>\d{1,3})\.\-\s+Η\sμε\sαριθμό\s(?P<id>\d{1,4})\/(?P<date>\d{1,2}\-\d{1,2}\-\d{1,4}).*?\s(?:του|της)\s(?:.*?\w+)(?P<person>.*?)(?: με θέμα:)?\s?\«(?P<title>.*?)\»\.'

        c1_group_name = False
        c2_group_name = False
        refs_group_name = False

        c1_groups = ['a100', 'a101', 'a110', 'a111']
        c2_groups = ['b100', 'b101', 'b110', 'b111']
        refs_groups = ['c100', 'c101', 'c110', 'c111']


        # loop and find what group name is catched in the regex
        for g in c1_groups:
            if wrapper_questions.group(g):
                c1_group_name = g
                break
        for g in c2_groups:
            if wrapper_questions.group(g):
                c2_group_name = g
                break
        for g in refs_groups:
            if wrapper_questions.group(g):
                refs_group_name = g
                break

        # if a_group_name is empty the question section is not found in the text
        if c1_group_name:
            for match in re.finditer(extract_regex, wrapper_questions.group(c1_group_name).strip(), flags=re.DOTALL|re.MULTILINE):
                c1_data = (
                    newsletter_date,
                    match.group('number').strip(),
                    match.group('id').strip(),
                    Scraper.convert_to_date_obj(match.group('date').strip().replace('-', '/')),
                    match.group('person').strip(),
                    match.group('title').strip(),
                    adl_id,
                )
                
                sql = """ INSERT INTO {} (
                            `qst_newsletter_date`,
                            `qst_numbering`,
                            `qst_id`,
                            `qst_addressed_date`,
                            `person_info`,
                            `qst_title`,
                            `adl_id`,
                            `type`)
                        VALUES (%s,%s,%s,%s,%s,%s,%s,'c1');""".format(self.top_questions_c1_table)
                
                self.cursor.execute(sql, c1_data)
            
        # if a_group_name is empty the question section is not found in the text
        if c2_group_name:
            for match in re.finditer(extract_regex, wrapper_questions.group(c2_group_name).strip()):
                c2_data = (
                    newsletter_date,
                    match.group('number').strip(),
                    match.group('id').strip(),
                    Scraper.convert_to_date_obj(match.group('date').strip().replace('-', '/')),
                    match.group('person').strip(),
                    match.group('title').strip(),
                    adl_id,
                )
                
                sql = """ INSERT INTO {} (
                            `qst_newsletter_date`,
                            `qst_numbering`,
                            `qst_id`,
                            `qst_addressed_date`,
                            `person_info`,
                            `qst_title`,
                            `adl_id`,
                            `type`)
                        VALUES (%s,%s,%s,%s,%s,%s,%s,'c2');""".format(self.top_questions_c2_table)
                
                self.cursor.execute(sql, c2_data)

        # if a_group_name is empty the question section is not found in the text
        if refs_group_name:
            for match in re.finditer(extract_regex, wrapper_questions.group(refs_group_name).strip()):
                
                refs_data = (
                    newsletter_date,
                    match.group('number').strip(),
                    match.group('id').strip(),
                    Scraper.convert_to_date_obj(match.group('date').strip().replace('-', '/')),
                    match.group('person').strip(),
                    match.group('title').strip(),
                    adl_id,
                )
                
                sql = """ INSERT INTO {} (
                            `qst_newsletter_date`,
                            `qst_numbering`,
                            `qst_id`,
                            `qst_addressed_date`,
                            `person_info`,
                            `qst_title`,
                            `adl_id`,
                            `type`)
                        VALUES (%s,%s,%s,%s,%s,%s,%s,'ref');""".format(self.references_questions_table)
                
                self.cursor.execute(sql, refs_data)

        print('commit:', newsletter_date)
        self.db_con.commit()

    @staticmethod
    def convert_to_date_obj(date_str: str=None) -> str:
        """Convert Date string to datetime object.

        Get a string in the form or 13/9/2000, split it on the slash character, do
        a list comprehension that converts the string values of the list to integers.

        Args:
            date_str: string.

        Returns:
            A datetime object.
        """
        if date_str is None:
            raise ValueError("date_str is not specified")

        if not isinstance(date_str, str):
            raise TypeError("date_str is not type string. Found: ", type(date_str))

        date_str = datetime.datetime.strptime(
            date_str, '%d/%m/%Y').strftime('%Y-%m-%d')
        date_str = datetime.datetime.strptime(
            date_str.replace('-', ''), "%Y%m%d").date()

        return date_str

    @staticmethod
    def get_number_of_pages_conferences() -> int:
        """ Get number of pages in hellenic parliament website """
        # Base url
        url = 'http://www.hellenicparliament.gr/Praktika/Synedriaseis-Olomeleias'

        # Make request to url
        r = requests.get(url, timeout=20)

        # Convert to soupObj
        soup = BeautifulSoup(r.text)

        # Find last page url
        last_page_url = soup.find(
            id='ctl00_ContentPlaceHolder1_rr_repSearchResults_ctl11_ctl01_repPager_ctl10_lnkLastPage').get('href')

        # Parse url parameters
        parsed = urlparse(last_page_url)

        # Get parameter pageNo. (page_number is list)
        page_number = parsed.query.split('=')[1]

        # Convert single list item to int and return value
        return int(page_number)

    @staticmethod
    def get_number_of_pages_top_questions(url: str=None) -> object:
        # Base url
        url = 'https://www.hellenicparliament.gr/Koinovouleftikos-Elenchos/Deltio-Epikairon-Erotiseon?search=on'

        # Make request to url
        r = requests.get(url, timeout=20)

        # Convert to soupObj
        soup = BeautifulSoup(r.text, 'lxml')

        # Find last page url
        last_page_url = soup.find(
            id='ctl00_ContentPlaceHolder1_pcsr1_repAgendaResults_ctl11_ctl00_repPager_ctl10_lnkLastPage').get('href')
        
        # Parse url parameters
        parsed = urlparse(last_page_url)
        
        # Get parameter pageNo. (page_number is list)
        page_number = parsed.query.split('=')[2]

        # Convert single list item to int and return value
        return int(page_number)

    @staticmethod
    def get_conference_page_bs4_table(url: str=None) -> object:
        if url is None:
            raise ValueError("No url scecified")

        if not isinstance(url, str):
            raise TypeError("Url is not integer. Found: ",
                            type(url))

        # A list that holds dictionaries
        # DELETE !  list_row_data = []

        # Scrape the HTML at the url
        r = requests.get(url)

        # Turn the HTML into a Beautiful Soup object
        soup = BeautifulSoup(r.text, 'lxml')
        
        # Create an object of the first object that is class=dataframe
        table = soup.find(class_='grid')

        if not table:
            raise ValueError("html table not found by BeautifulSoup")

        return table

    @staticmethod
    def get_top_questions_page_bs4_table(url: str=None) -> object:
        if url is None:
            raise ValueError("No url scecified")

        if not isinstance(url, str):
            raise TypeError("Url is not integer. Found: ",
                            type(url))

        r = requests.get(url)

        # Turn the HTML into a Beautiful Soup object
        soup = BeautifulSoup(r.text, 'lxml')

        # Create an object of the first object that is class=dataframe
        table = soup.find(id='pagecontent')

        return table

    def merge_same_days(self):
        """ Find files that are morning or noon and merge them to one file.

        Personal note to me:
            The merging functions are shit.
            Fix the fucking logic...
        """

        morning_files = []
        noon_files = []
        not_morning_noon_files = []
        tmp_all_files = files.get_files_from_folder(self.output_folder)

        for f in tmp_all_files:
            if 'morning' in f:
                morning_files.append(f)

            elif 'noon' in f:
                noon_files.append(f)

        # Keep only files that are not morning or noon
        for a in tmp_all_files:
            if 'morning' not in a and 'noon' not in a:
                not_morning_noon_files.append(a)

        for m in morning_files:
            # sample /home/greekparliament/parser/workspace/text/2018-05-09morning.docx
            date_ = m.split('/')[-1].split('morning')[0]
            for nmnf in not_morning_noon_files:
                if(date_ in nmnf):
                    print('found match morning: ', m, nmnf)
                    self.merge_two_files(m, nmnf)

        for n in noon_files:
            # sample /home/greekparliament/parser/workspace/text/2018-05-09noon.docx
            date_ = n.split('/')[-1].split('noon')[0]
            for nmnf in not_morning_noon_files:
                if(date_ in nmnf):
                    print('found match noon: ', n, nmnf)
                    self.merge_two_files(n, nmnf)

        # Merge files that have a morning and noon conference at the same day
        for m in morning_files:
            # sample /home/greekparliament/parser/workspace/text/2018-05-09morning.docx
            date_ = m.split('/')[-1].split('morning')[0]
            for n in noon_files:
                if date_ in n:
                    print('found match morning_noon')
                    self.merge_morning_and_noon(m, n)

    def merge_two_files(self, file1: str=None, file2: str=None) -> None:
        """ Merge file1 to file2.

        file1 must be the file with the morning or noon tag
        """
        if not file1:
            raise ValueError('file1 not specified')
        if not file2:
            raise ValueError('file2 not specified')
        if 'morning' in file2 or 'noon' in file2:
            raise ValueError('file with morning or noon tag must be the first argument')

        print('File1: {}\nFile2: {}'.format(file1, file2))

        filenames = [file1, file2]

        # Use temp folder because open is opening the file used in for loop
        # the file must be deleted after the merge else there will be no file
        # to read from THE FOLDER /tmp MUST EXIST!!!
        output_file = self.output_folder + '/tmp/' + file2.split('/')[-1].split('.')[0] + '.txt'
        with open(output_file, 'w') as outfile:
            for fname in filenames:
                # used for converting doc & docx files -_-
                infile = self.converter.extract_text(fname)
                for line in infile:
                    outfile.write(line)
                print('File merged')

            # Remove the old files
            os.remove(file1)
            os.remove(file2)

            # Get path
            one_level_up_path = re.search(r'(.*\/).*\/', output_file)

            # Move file one level up
            print(one_level_up_path.group(1))
            print(output_file)
            shutil.move(output_file, one_level_up_path.group(1))

    def merge_morning_and_noon(self, file1: str=None, file2: str=None) -> None:
        """ Merge a morning file with a noon file. """
        if not file1:
            raise ValueError('file1 not specified')
        if not file2:
            raise ValueError('noon not specified')
        if 'morning' not in file1:
            raise ValueError('first argument must have a morning file')
        if 'noon' not in file2:
            raise ValueError('second argument must have a noon file')

        filenames = [file1, file2]

        # Use temp folder because open is opening the file used in for loop
        # the file must be deleted after the merge else there will be no file
        # to read from
        output_file = self.output_folder + '/tmp/' + file2.split('/')[-1].split('.')[0] + '.txt'
        with open(output_file, 'w') as outfile:
            for fname in filenames:
                # used for converting doc & docx files -_-
                infile = self.converter.extract_text(fname)
                for line in infile:
                    outfile.write(line)
                print('File merged')

            # Remove the old files
            os.remove(file1)
            os.remove(file2)

            # Get path
            one_level_up_path = re.search(r'(.*\/).*\/', output_file)

            # Move file one level up
            print(one_level_up_path.group(1))
            print(output_file)
            shutil.move(output_file, one_level_up_path.group(1))

    def rename_stray_morning_noon(self):
        """ Rename stray morning or noon files. """
        print("Renaming stray morning/noon files...")
        all_files = files.get_files_from_folder(self.output_folder)

        for f in all_files:
            if 'morning' in f:
                os.rename(f, f.replace('morning', ''))

            elif 'noon' in f:
                os.rename(f, f.replace('noon', ''))
