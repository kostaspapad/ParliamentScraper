# -*- coding: utf-8 -*-
import sys
import time
from bin.scraper import scraper as Spr

def main():

    print("Starting crawler...")
    req_version = (3, 0)
    cur_version = sys.version_info

    if cur_version >= req_version:

        scraper = Spr.Scraper()
        # scraper.crawl_all_top_questions()
        # scraper.crawl_top_questions(1)
        # exit()
        # Crawl everyting        
        # scraper.crawl_all_conferences()

        # Cronjob
        scraper.crawl_for_new() # For cronjob functionality (conferences and top_questions)
        scraper.download_not_downloaded()
        scraper.merge_same_days()
        scraper.rename_stray_morning_noon()
        scraper.import_conferences_to_db()
        
        # for debugging
        # scraper.crawl_page(380) # 380 has morning conference
        # scraper.download_by_id(796)
        # scraper.merge_two_files(
        #    '/home/greekparliament/parser/workspace/text/2001-03-14morning.txt',
        #    '/home/greekparliament/parser/workspace/text/2001-03-14.txt')
        print('Done')
        
if __name__ == "__main__":
    main()